<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">

    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/user.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Italianno|Lato:400,700,900|Raleway:400,700,900" rel="stylesheet">
    <title>Strolf</title>

    @yield('style')
  </head>
  <body style="background-color: #e6ecff">
    <header class="site-header container">
        <div class="row justify-content-between">
              <div class="col-8 offset-2 col-lg-4 offset-lg-0">
                  <a href="index.php">
                      <img src="/img/logo2.png" class="/img-fluid mx-auto d-block">
                  </a>
              </div>
              <div class="col-12 col-lg-4">
                  <nav class="socials text-center text-md-right pt-3">
                    <ul class="">
                        <li><a href="http://facebook.com"><span class="sr-only">Facebook</span></a></li>
                        <li><a href="http://twitter.com"><span class="sr-only">Twitter</span></a></li>
                        <li><a href="http://instagram.com"><span class="sr-only">Instagram</span></a></li>
                        <li><a href="http://pinterest.com"><span class="sr-only">Pinterest</span></a></li>
                        <li><a href="http://youtube.com"><span class="sr-only">Youtube</span></a></li>
                    </ul>

                  </nav>
              </div>
        </div> <!--.justify-content-between-->
    </header>
<div class="navigation mt-4 py-1">
    <nav class="navbar main-nav navbar-expand-sm navbar-light bg-faded ">

    <!-- Links -->
    <ul class="nav nav-justified flex-column flex-sm-row">
      <li class="nav-item">
        <a class="nav-link" href="/member">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/member/transactions">Transactions</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="/member/payment">Payment</a>
      </li>
      <!-- Dropdown -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
            Profile
        </a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="/member/profile" >Profile</a>
          <a class="dropdown-item" href="{{ url('/logout') }}"
              onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" >Logout</a>
        </div>
      </li>
    </ul>
  </nav>
</div>
<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
  <br>


          </div>
    </header>
<!-- Payment Modal-->
<div class="container">
  <div class="col-md-4"></div>
  <div class="col-8 col-md-4">
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
      <div class="modal-content">
        <div class="modal-header">
          <h2 class="modal-title">Input valid mpesa number to prompt Payment</h2>
        </div>
        <div class="modal-body">
          <form class="p-5 mt-5 contact-form" action="#" method="post">
            <div class="form-group">
              <label class="form-group">Number</label>
              <input type="number" class="form-control" name="name" id="name" value="">
            </div>

          </form>
        </div>
        <div class="modal-footer">


            <input type="submit" class="btn btn-primary text-uppercase" name="submit" value="submit">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>
<div class="col-md-4"></div>


<!-- <div class="navigation mt-4 py-1">
<nav class="main-nav py-1 navbar navbar-toggleable-sm navbar-light bg-faded ">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
    data-target="#main-navigation" aria-expanded="false" aria-label="Toggle Navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a href="#" class="navbar-brand d-sm-none">Strolf</a>
    <div class="container">
            <div class="collapse navbar-collapse" id="main-navigation">
                <ul class="nav nav-justified flex-column flex-sm-row">
                    <li class="nav-item">
                          <a href="#" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item">
                          <a href="#" class="nav-link">Transactions</a>
                    </li>
                    <li class="nav-item">
                          <a href="#" class="nav-link">Payment</a>
                    </li>
                    <li class="nav-item">
                          <a href="#" class="nav-link">Profile</a>
                    </li>

                </ul>
            </div>
    </div>
</nav>
</div> -->

    @yield('content')

    <footer class="site-footer pt-5 bg-bootstrap">
    <div class="container">
        <div class="row">
              <div class="col-md-4">
                  <h3 class="text-uppercase text-center pb-4">About Us</h3>
                  <p class="text-justify">Praesent pulvinar dolor eu metus mollis egestas. Vestibulum efficitur, magna quis laoreet rutrum, eros ipsum rutrum erat, a facilisis neque dui ullamcorper metus.</p>
              </div>
              <div class="col-md-4 pb-4 pb-md-0">
                  <h3 class="text-uppercase text-center pb-4">Open hours</h3>
                  <p class="text-center mb-0">Mon-Fri: 9 AM - 7 PM</p>
                  <p class="text-center mb-0">Saturday: 10 AM - 2 PM</p>
                  <p class="text-center mb-0">Sunday: Closed</p>
              </div>
              <div class="col-md-4 ">
                  <h3 class="text-uppercase text-center pb-4">Contact</h3>
                  <p class="mb-0">Strathmore University </p>
                  <p>Golf Club</p>

                  <div class="social-nav">
                    <nav class="socials text-left pt-3">
                      <ul class="p-0">
                          <li><a href="http://facebook.com"><span class="sr-only">Facebook</span></a></li>
                          <li><a href="http://twitter.com"><span class="sr-only">Twitter</span></a></li>
                          <li><a href="http://instagram.com"><span class="sr-only">Instagram</span></a></li>
                          <li><a href="http://pinterest.com"><span class="sr-only">Pinterest</span></a></li>
                          <li><a href="http://youtube.com"><span class="sr-only">Youtube</span></a></li>
                      </ul>

                    </nav>
                  </div>
              </div>

              <div class="w-100"></div>
              <hr class="w-100">

              <p class="copyright text-center w-100">Strolf</p>
        </div> <!--.row-->
    </div> <!--.container-->

    @yield('scripts')
</footer>


    <script src="/assets/vendor/jquery/dist/jquery.min.js" ></script>
    <script src="	https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/tether.min.js"></script>
    <script src="/js/scripts.js"></script>

  </body>
</html>
