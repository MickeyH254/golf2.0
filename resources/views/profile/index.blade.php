<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<hr>
<div class="container bootstrap snippet">
    <div class="row">
  		<div class="col-sm-10"><h1>{{$user->name}}</h1></div>
    	<div class="col-sm-2"><a href="/users" class="pull-right"></a></div>
    </div>
    <div class="row">
      <div class="col-sm-3"></div>
  		<div class="col-sm-6"><!--left col-->




      <form class="form" action="/user/{{$user->id}}" method="post" id="registrationForm" enctype="multipart/form-data">
        <div class="text-center">
          <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle img-thumbnail" alt="avatar">
          <h6>Upload a different photo...</h6>
          <input type="file" name="image" class="text-center center-block file-upload">
        </div></hr><br>

        {{csrf_field()}}
          <div class="form-group">

              <div class="col-xs-6">
                  <label for="first_name"><h4>User name</h4></label>
                  <input type="text" class="form-control" name="name" id="first_name" value="{{$user->name}}" placeholder="first name" title="enter your first name if any.">
              </div>
          </div>

          <div class="form-group">

              <div class="col-xs-6">
                  <label for="phone"><h4>Phone</h4></label>
                  <input type="text" class="form-control" name="phone" value="" id="phone" placeholder="enter phone" title="enter your phone number if any.">
              </div>
          </div>


          <div class="form-group">

              <div class="col-xs-6">
                  <label for="email"><h4>Email</h4></label>
                  <input type="email" class="form-control" name="email" id="email" value="{{$user->email}}" placeholder="you@email.com" title="enter your email.">
              </div>
          </div>
          <div class="form-group">

              <div class="col-xs-6">
                  <label for="email"><h4>Date of Birth</h4></label>
                  <input type="date" class="form-control" id="location" name="date_of_birth" value="{{$user->date_of_birth}}" placeholder="somewhere" title="enter a location">
              </div>
          </div>
          <div class="form-group">

              <div class="col-xs-6">
                  <label for="email"><h4>Occupation</h4></label>
                  <input type="text" class="form-control" id="location" name="Occupation" value="{{$user->Occupation}}" placeholder="somewhere" title="enter a location">
              </div>
          </div>
          <div class="form-group">

              <div class="col-xs-6">
                  <label for="password"><h4>Password</h4></label>
                  <input type="password" class="form-control" name="password" id="password" placeholder="password" title="enter your password.">
              </div>
          </div>
          <div class="form-group">

              <div class="col-xs-6">
                <label for="password2"><h4>Verify</h4></label>
                  <input type="password" class="form-control" name="password2" id="password2" placeholder="password2" title="enter your password2.">
              </div>
          </div>
          <div class="form-group">
               <div class="col-xs-12">
                    <br>
                    <button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                    <button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                </div>
          </div>
    </form>

        </div><!--/col-3-->

    </div><!--/row-->
  </div>
  <script type="text/javascript">
  $(document).ready(function() {


  var readURL = function(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('.avatar').attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]);
      }
  }


  $(".file-upload").on('change', function(){
      readURL(this);
  });
});
  </script>
</body>
