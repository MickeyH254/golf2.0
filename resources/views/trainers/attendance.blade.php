@extends('layouts.trainers')

@section('header')
<script src="assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
@endsection

@section('content')
<div class="col">
  <h2 class="text-uppercase text-white mb-0">Add attendance</h2>
</div>

<div class="form-group" style="margin-top: 40px">
    <div class="input-group input-group-alternative">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
        </div>
        <input id="attendance_date" class="form-control datepicker"  placeholder="Select date" type="text">
        <span title="Add Attendace" class="input-group-text" data-toggle="modal" data-target="#modal-default"><i class="ni ni-fat-add text-orange" style="font-size: 25px "></i></span>
    </div>
</div>

<div class="col">
  <h2 class="text-uppercase text-green mb-0">Attendace on Date: <span id="header_date"></span></h2>
</div>

<div class="table-responsive">
  <table id="attendance_table" class="table align-items-center table-flush">
    <thead class="thead-light">
      <tr>
        <th scope="col">Id</th>
        <th scope="col">Name</th>
        <th scope="col">Attendace</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          1
        </td>
        <td>
          <span class="badge badge-dot mr-4">
            <i class="bg-warning"></i> Mike
          </span>
        </td>
        <td>
          <div class="d-flex align-items-center">
            <span class="mr-2">60%</span>
            <div>
              <div class="progress">
                <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
              </div>
            </div>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
</div>
<!-- Attendace modal -->

<div class="row">
  <div class="col-md-6">
      <div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default">Attendace</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">
              <div class="table-responsive">
                <table id="modal_table" class="table align-items-center table-flush">
                  <thead class="thead-light">
                    <tr>
                      <th scope="col">Id</th>
                      <th scope="col">Member</th>
                      <th scope="col">Present</th>
                      <th scope="col">Absent</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">
                          <span class="mb-0 text-sm text-green">1</span>
                      </th>
                      <td>
                        Mike
                      </td>
                      <td>
                        <div class="custom-control custom-radio mb-3">
                          <input name="custom-radio-1" class="custom-control-input" id="customRadio1" type="radio">
                          <label class="custom-control-label" for="customRadio1">Present</label>
                        </div>
                      </td>
                      <td>
                        <div class="custom-control custom-radio mb-3">
                          <input name="custom-radio-1" class="custom-control-input" id="customRadio2" type="radio">
                          <label class="custom-control-label" for="customRadio2">Absent</label>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            <div class="modal-footer">
                <button type="button" id="savechanges" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
  </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
var dat=new Date();
var att_ar=[];
$("#savechanges").on('click',function () {
  $tr=$("#modal_table tbody").children('tr');
  $tr.each(function(){
    $this=$(this);
    console.log($this.find('input[name="att_id"]').val());
  });
});
    // var attendance = document.getElementById('attendance_date').value;
    // document.getElementById('header_date').innerHTML = attendance;
    // console.log(attendance);

    // setInterval(function() {
    //    var attendance = ObserveInputValue($('#attendance_date').val());
    //    $('#header_date').text(attendance);
    //  }, 100);
    jQuery('#attendance_date').on('change', function() {
      var attendance = $('#attendance_date').val();
      $('#header_date').text(attendance);
      var settings = {
  "async": true,
  "crossDomain": true,
  "url": "/attendees/?date="+attendance,
  "method": "GET",
  "headers": {
    "cache-control": "no-cache",
    "Postman-Token": "37c4af16-56db-4afe-a7a5-ddad23d97121"
  }
}

$.ajax(settings).done(function (response) {
  att_ar=[];
  var markup;
  var markup1;
  if(JSON.parse(response).users.length!=0 || JSON.parse(response).absentees.length!=0){
  var att1=JSON.parse(response).absentees;
  var att=JSON.parse(response).users;
  // console.log(att1);


  var counter=1;
  $.each(att, function( index, value ) {
    att_ar[counter]=att[index].id;
    markup += "<tr><td>"+(counter)+"</td><td>" + att[index].name + "</td><td>Present</td></tr>";
    markup1 += '<tr><th scope="row"> <span class="mb-0 text-sm text-green">'+(counter)+'</span> </th> <td> '+ att[index].name + '</td> <td> <div class="custom-control custom-radio mb-3"> <input type="hidden" name="att_id" value="'+att[index].att_id+'"/><input name="custom-radio-1" checked class="custom-control-input" id="customRadio1" onclick="presentForPresent(this)" type="radio"> <label class="custom-control-label" for="customRadio1">Present</label> </div> </td> <td> <div class="custom-control custom-radio mb-3"> <input name="custom-radio-1" onclick="absentForPresent(this)" class="custom-control-input"  id="customRadio2" type="radio"> <label class="custom-control-label" for="customRadio2">Absent</label> </div> </td> </tr>';
    counter++;
});
$.each(att1, function( index, value ) {
  att_ar[counter]=att1[index].id;
  markup += "<tr><td>"+(counter)+"</td><td>" + att1[index].name + "</td><td>Present</td></tr>";
  counter++;
  markup1 += '<tr><th scope="row"> <span class="mb-0 text-sm text-green">'+(counter)+'</span> </th> <td> '+ att1[index].name + '</td> <td> <div class="custom-control custom-radio mb-3"> <input name="custom-radio-1"  class="custom-control-input" id="customRadio1" type="radio"> <label class="custom-control-label" for="customRadio1">Present</label> </div> </td> <td> <div class="custom-control custom-radio mb-3"> <input name="custom-radio-1" checked class="custom-control-input"  id="customRadio2" type="radio"> <label class="custom-control-label" for="customRadio2">Absent</label> </div> </td> </tr>';
});}else {
  markup+="<tr><td>There are no subscribers for this month</td></tr>";
  markup1+="<tr><td>There are no subscribers for this month</td></tr>";
}
$("#attendance_table tbody").html(markup);
$('#modal_table tbody').html(markup1);
});
});
$('#attendance_date').val((dat.getMonth()+1)+"/"+(dat.getDate())+"/"+(dat.getFullYear()));
$('#attendance_date').trigger('change');
function presentForPresent(ob) {
  $this=$(ob);
  console.log($this.parent().find('input[name="att_id"]').val());
}
function absentForPresent(ob) {
  $this=$(ob);
  console.log($this.parent().parent().parent().find('input[name="att_id"]').val());
}
function presentForAbsent(ob) {
  $this=$(ob);
  // console.log($this.parent().find('input[name="att_id"]').val());
}
function absentForAbsent(ob) {
  $this=$(ob);
  // console.log($this.parent().find('input[name="att_id"]').val());
}
</script>
@endsection
