@extends('layouts.trainers')

@section('content')
<div class="row">
<div class="col">
  <div class="card shadow">
    <div class="card-header border-0">
      <h3 class="mb-0">Members</h3>
    </div>
    <div class="table-responsive">
      <table class="table align-items-center table-flush">
        <thead class="thead-light">
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Status</th>
            <th scope="col">Paid</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">
              <div class="media align-items-center">
                <a href="#" class="avatar rounded-circle mr-3">
                  <img alt="Image placeholder" src="../assets/img/theme/bootstrap.jpg">
                </a>
                <div class="media-body">
                  <span class="mb-0 text-sm">Strolf</span>
                </div>
              </div>
            </th>
            <td>
              $2,500 USD
            </td>
            <td>
              <span class="badge badge-dot mr-4">
                <i class="bg-warning"></i> pending
              </span>
            </td>
            <td>
              <div class="d-flex align-items-center">
                <span class="mr-2">60%</span>
                <div>
                  <div class="progress">
                    <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                  </div>
                </div>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="card-footer py-4">
      <nav aria-label="...">
        <ul class="pagination justify-content-end mb-0">
          <li class="page-item disabled">
            <a class="page-link" href="#" tabindex="-1">
              <i class="fas fa-angle-left"></i>
              <span class="sr-only">Previous</span>
            </a>
          </li>
          <li class="page-item active">
            <a class="page-link" href="#">1</a>
          </li>
          <li class="page-item">
            <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
          </li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item">
            <a class="page-link" href="#">
              <i class="fas fa-angle-right"></i>
              <span class="sr-only">Next</span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
</div>
</div>
<!-- Delete Alert -->
<!-- Execute if user is deleted -->
<div class="alert alert-warning" role="alert">
    <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
    <span class="alert-inner--text">User deleted</span>
</div>

@endsection
