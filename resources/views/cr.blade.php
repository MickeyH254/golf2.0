@component('mail::message')
# Introduction

Payment has been made by {{$user->name}} from {{start_date}} to {{expiry_date}}

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
