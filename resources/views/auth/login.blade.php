@extends('layouts.app')

@section('content')
<div class="card-body px-lg-5 py-lg-5">
  <div class="text-center text-muted mb-4">
    <p style="font-size: 20px">Sign in with credentials</p>
  </div>
  <form role="form" action="{{ url('/login') }}" method="post">
    {{csrf_field()}}
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
    <div class="form-group mb-3">
      <div class="input-group input-group-alternative">
        <div class="input-group-prepend">
          <span class="input-group-text"><i class="ni ni-email-83"></i></span>
        </div>
        <input class="form-control" placeholder="Email" name="email" type="email">
      </div>
    </div>
    <div class="form-group">
      <div class="input-group input-group-alternative">
        <div class="input-group-prepend">
          <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
        </div>
        <input class="form-control" name="password" placeholder="Password" type="password">
      </div>
    </div>
    <div class="custom-control custom-control-alternative custom-checkbox">
      <input class="custom-control-input" name="remember_token" id=" customCheckLogin" type="checkbox">
      <label class="custom-control-label" for=" customCheckLogin">
        <span class="text-muted" >Remember me</span>
      </label>
    </div>
    <div class="text-center">
      <button type="submit" class="btn btn-primary my-4">Sign in</button>
    </div>
  </form>
</div>

@endsection

@section('details')
<div class="row mt-3">
  <div class="col-6">
    <a href="#" class="text-warning"><small>Forgot password?</small></a>
  </div>
  <div class="col-6 text-right">
    <a href="register" class="text-warning"><small>Create new account</small></a>
  </div>
</div>
@endsection
