@extends('layouts.app')

@section('content')
<div class="card-body px-lg-5 py-lg-5">
  <div class="text-center text-muted mb-4">
    <p style="font-size: 20px">Create a new account</p>
  </div>
  <form role="form" action="register" method="post">
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach

    {{csrf_field()}}
    <div class="form-group">
      <div class="input-group input-group-alternative mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text"><i class="ni ni-hat-3"></i></span>
        </div>
        <input class="form-control" name="name" placeholder="Name" type="text">
      </div>
    </div>
    <div class="form-group mb-3">
      <div class="input-group input-group-alternative">
        <div class="input-group-prepend">
          <span class="input-group-text"><i class="ni ni-email-83"></i></span>
        </div>
        <input class="form-control" name="email" placeholder="Email" type="email">
      </div>
    </div>
    <div class="form-group">
      <div class="input-group input-group-alternative">
        <div class="input-group-prepend">
          <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
        </div>
        <input class="form-control" name="password" placeholder="Password" type="password">
      </div>
    </div>
    <div class="form-group">
      <div class="input-group input-group-alternative">
        <div class="input-group-prepend">
          <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
        </div>
        <input class="form-control" name="password_confirmation" placeholder="Password" type="password">
      </div>
    </div>
    <div class="row my-4">
      <div class="col-12">
        <div class="custom-control custom-control-alternative custom-checkbox">
          <input class="custom-control-input" id="customCheckRegister" type="checkbox">
          <label class="custom-control-label" for="customCheckRegister">
            <span class="text-muted">I agree with the <a href="#!">Privacy Policy</a></span>
          </label>
        </div>
      </div>
    </div>

    <div class="text-center">
      <button type="submit" class="btn btn-primary my-4">Create Account</button>
    </div>
  </form>
</div>
@endsection

@section('details')
<div class="row mt-3">
  <div class="col-6">
    <a href="#" class="text-warning"><small>Forgot password?</small></a>
  </div>
  <div class="col-6 text-right">
    <a href="login" class="text-primary"><small>Already have an account? Log in</small></a>
  </div>
</div>
@endsection
