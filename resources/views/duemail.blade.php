@component('mail::message')
# Introduction

your gym payment is due tommorow.

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
