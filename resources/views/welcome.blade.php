<!DOCTYPE html>
<html>
<head>
    <title>Golf System</title>

    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link></link>
</head>
<body>
  <header>
    <nav>
      <div class="row">
        <img src="#" alt="golf_logo" class="golf_logo">
          <ul class="the_navbar">
            <li href="#">Home</li>
            <li href="#">Login/Register</li>
            <li>
              <a href="{{ url('/logout') }}"
                  onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                  Logout
              </a>

              <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>
            </li>
          </ul>
      </div>
    </nav>
    <div class="hero-text-box">
        <h1>Welcome to the strolf</h1>
        <a class="btn btn-full" href="login">Login </a>
        <a class="btn btn-ghost" href="register">Register</a>
        <a href="{{ url('/logout') }}"
            onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
            Logout
        </a>
    </div>
  </header>
</body>
</html>
