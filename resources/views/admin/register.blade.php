@extends('layouts.admin')

@section('content')
     <!-- Table -->
     <div class="row justify-content-center">
       <div class="col-lg-6 col-md-8">
         <div class="card bg-secondary shadow border-0">
           <div class="card-header bg-transparent pb-5">
             <div class="text-muted text-center mt-2 mb-4"><small>Create a new User</small></div>
           </div>
           <div class="card-body px-lg-5 py-lg-5">
             <form role="form" action="/adduser" method="post">
               @foreach($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach

               {{csrf_field()}}
               <div class="form-group">
                 <div class="input-group input-group-alternative mb-3">
                   <div class="input-group-prepend">
                     <span class="input-group-text"><i class="ni ni-hat-3"></i></span>
                   </div>
                   <input class="form-control" name="name" placeholder="Name" type="text">
                 </div>
               </div>
               <div class="form-group mb-3">
                 <div class="input-group input-group-alternative">
                   <div class="input-group-prepend">
                     <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                   </div>
                   <input class="form-control" name="email" placeholder="Email" type="email">
                 </div>
               </div>
               <div class="form-group">
                 <div class="input-group input-group-alternative">
                   <div class="input-group-prepend">
                     <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                   </div>
                   <input class="form-control" name="password" placeholder="Password" type="password">
                 </div>
               </div>
               <div class="form-group">
                 <div class="input-group input-group-alternative">
                   <div class="input-group-prepend">
                     <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                   </div>
                   <input class="form-control" name="password_confirmation" placeholder="Password" type="password">
                 </div>
               </div>
               <div class="form-group">
                 <div class="input-group input-group-alternative">
                   <div class="input-group-prepend">
                     <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                   </div>
                   <select class="" name="role_id">
                     @foreach($roles as $role)
                     <option value="{{$role->id}}">{{$role->name}}</option>
                     @endforeach

                   </select>

                 </div>
               </div>
               <div class="row my-4">
                 <div class="col-12">
                   <div class="custom-control custom-control-alternative custom-checkbox">
                     <input class="custom-control-input" id="customCheckRegister" type="checkbox">
                     <label class="custom-control-label" for="customCheckRegister">
                       <span class="text-muted">I agree with the <a href="#!">Privacy Policy</a></span>
                     </label>
                   </div>
                 </div>
               </div>

               <div class="text-center">
                 <button type="submit" class="btn btn-primary my-4">Create Account</button>
               </div>
             </form>
           </div>
         </div>
       </div>
     </div>
   </div>
@endsection
