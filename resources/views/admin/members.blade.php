@extends('layouts.admin')

@section('content')
<script type="text/javascript">
$(document).on("click", "#edit", function () {
   var row = $(this).closest('tr');
   var $tds=row.find('.dat');
   var tdata=[];
   $.each($tds, function() {               // Visits every single <td> element
    tdata.push($(this).text());        // Prints out the text within the <td>
});
   $("#modal_name").val( tdata[0] );
   $("#modal_email").val( tdata[1]);
   $("#modal_number").val(  tdata[2]);

   // As pointed out in comments,
   // it is superfluous to have to manually call the modal.
   // $('#edit_modal').modal('show');
});
</script>
<div class="row">
<div class="col">
  <div class="card shadow">
    <div class="card-header border-0">
      <h3 class="mb-0">Users</h3>
    </div>
    <div class="table-responsive">
      <table class="table align-items-center table-flush">
        <thead class="thead-light">
          <tr>

            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">PhoneNumber</th>
            <th scope="col">Status</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          @foreach($members as $member)
          <tr>
            <td scope="row">
                <a href="#" >
                  <div class="media align-items-center">
                  <img alt="Image placeholder" class="avatar rounded-circle mr-3" src="img/avatar.png">
                </a>
                <div class="media-body">
                  <span class="dat mb-0 text-sm">{{$member->name}}</span>
                </div>
              </div>
            </td>
            <td class="dat">
              {{$member->email}}
            </td>
            <td class="dat">
              {{$member->phone?$member->phone:"n/a"}}
            </td>
            <td>
              <span class="dat badge badge-dot mr-4">
                <i class="bg-warning"></i> {{in_array($member,$paidmembers)?'paid':'pending'}}
              </span>
            </td>
            <!-- <td>
              <div class="d-flex align-items-center">
                <span class="mr-2">60%</span>
                <div>
                  <div class="progress">
                    <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                  </div>
                </div>
              </div>
            </td> -->
            <td class="text-right">
              <div class="dropdown">
                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-ellipsis-v"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                  <a class="dropdown-item" id="edit" href="#" data-toggle="modal" data-target="#modal-form">Edit</a>
                  <a class="dropdown-item" href="#">Delete</a>
                </div>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="card-footer py-4">
      <nav aria-label="...">
        <ul class="pagination justify-content-end mb-0">
          <li class="page-item disabled">
            <a class="page-link" href="#" tabindex="-1">
              <i class="fas fa-angle-left"></i>
              <span class="sr-only">Previous</span>
            </a>
          </li>
          <li class="page-item active">
            <a class="page-link" href="#">1</a>
          </li>
          <li class="page-item">
            <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
          </li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item">
            <a class="page-link" href="#">
              <i class="fas fa-angle-right"></i>
              <span class="sr-only">Next</span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
</div>
</div>
<!-- Delete Alert -->
<!-- Execute if user is deleted -->
<div class="alert alert-warning" role="alert">
    <span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
    <span class="alert-inner--text">User deleted</span>
</div>

<!-- Edit Modal -->
<div class="row" id="edit_modal">
<div class="col-md-4">
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
<div class="modal-content">

    <div class="modal-body p-0">

<div class="card bg-secondary shadow border-0">
    <div class="card-body px-lg-5 py-lg-5">
        <div class="text-center text-muted mb-4">
            <h3>Edit User</h3>
        </div>
        <form role="form" action="action('UserController@update')" method="post">
            <div class="form-group mb-3">
                <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input class="form-control" placeholder="Name" id="modal_name" type="text">
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input class="form-control" id="modal_email" placeholder="Email" type="Email">
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input class="form-control" id="modal_number" placeholder="Phone Number" type="number">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                    </div>
                    <input class="form-control" id="modal_password" placeholder="Password" type="password">
                </div>
            </div>

            <div class="text-center">
                <button type="button" class="btn btn-primary my-4">Update</button>
            </div>
        </form>
    </div>
</div>



            </div>

        </div>
    </div>
</div>
  </div>
</div>
@endsection
