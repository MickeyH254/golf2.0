@extends('layouts.admin')


@section('header')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js
"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js
"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js
"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js
"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js
"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js
"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js
"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js
"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

<script>
$(document).ready(function() {
    // $('#example').DataTable( {
    //     dom: 'Bfrtip',
    //     buttons: [
    //         'csv', 'pdf', 'print'
    //     ]
    // } );
} );
</script>
@endsection

@section('content')
<div class="row">
<div class="col">
  <div class="card shadow">
    <div class="card-header border-0">
      <h3 class="mb-0">Transactions table</h3>
    </div>
    <div class="table-responsive">
      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Transaction id</th>
                <th>Transaction type</th>
                <th>user</th>
                <th>Date</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
          @foreach($payments as $payment)
            <tr>
                <td>{{$payment->id}}</td>
                <td>{{$payment->mode}}</td>
                <td>{{\App\User::find($payment->user_id)->name}}</td>
                <td>{{$payment->created_at}}</td>
                <td>{{\App\Subscription::find($payment->subscription_id)->price}}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
              <th>Transaction id</th>
              <th>Transaction type</th>
              <th>user</th>
              <th>Date</th>
              <th>Amount</th>
            </tr>
        </tfoot>
      </table>
    </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
@endsection
