@extends('layouts.user')

@section('style')
<style media="screen">

.card {
box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
max-width: 300px;
margin: auto;
text-align: center;
font-family: arial;
}

.price {
color: grey;
font-size: 22px;
}

.card button {
border: none;
outline: 0;
padding: 12px;
color: white;
background-color: #000;
text-align: center;
cursor: pointer;
width: 100%;
font-size: 18px;
}

.card button:hover {
opacity: 0.7;
}
</style>

<link href="/assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
<link href="/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
<!-- Argon CSS -->
<link href='/assets/vendor/fullcalendar.css' rel='stylesheet' />
<link href='/css/fullcalendar.print.css' rel='stylesheet' media='print' />

 <link type="text/css" href="/assets/css/argon.css?v=1.0.0" rel="stylesheet">
<script src="/assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="/assets/js/argon.js?v=1.0.0"></script>
<script type="text/javascript">
  var subscription_id;
  function subby(z) {
    $("#sub_id").val(z);
  }

</script>


@endsection

@section('content')
<div id="demo" class="carousel slide" data-ride="carousel">

  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="/img/login_golf.jpg" alt="" width="1100" height="500">
    </div>
  </div>
</div>

<div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3 py-3 text-center">
  <h3 class="text-uppercase">Choose your payment plan</h3>
</div>

<div class="container" style="margin-top: 40px">
  <div class="row">
  <div class="col-12 col-md-6">
    <div class="card">
      <img src="/img/golf-new.jpg" alt="" style="width:100%">
      <h1>Training without trainer</h1>
      <p class="price">Ksh 2500</p>
      <p></p>
      <p><button id="1st" data-toggle="modal" data-target="#mpesaModal" onclick="subby(1)">Pay for membership</button></p>
    </div>
  </div>
  <div class="col-12 col-md-6">
    <div class="card">
      <img src="/img/golf-train.jpg" alt="" style="width:100%">
      <h1>Training with trainer</h1>
      <p class="price">Ksh 3500</p>
      <p></p>
      <p><button id="2nd" data-toggle="modal" data-target="#mpesaModal" onclick="subby(2)">Pay for membership</button></p>
    </div>
  </div>
</div>
</div>

<!-- Payment Modal -->
<div class="row">
<div class="col-md-4">
    <div class="modal fade" id="mpesaModal" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
<div class="modal-content">

    <div class="modal-body p-0">

<div class="card bg-secondary shadow border-0">
    <div class="card-body px-lg-5 py-lg-5">
        <div class="text-center text-muted mb-4">
            <h3>Enter a valid mpesa number</h3>
        </div>
        <form role="form" action="/mpesa" method="post">
          {{csrf_field()}}
            <div class="form-group mb-3">
                <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-phone"></i></span>
                    </div>
                    <input id="sub_id" class="form-control" name="subscription_id" type="hidden">
                    <input class="form-control" placeholder="Number: 2547xxxxxxxx" name="cust_phone" type="number">
                </div>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary my-4">Pay with Mpesa</button>
            </div>
        </form>
    </div>
</div>



            </div>

        </div>
    </div>
</div>
  </div>
</div>


<!-- Mpesa Modal with training -->
<div class="row">
<div class="col-md-4">
    <div class="modal fade" id="mpesaModalTrain" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
<div class="modal-content">

    <div class="modal-body p-0">

<div class="card bg-secondary shadow border-0">
    <div class="card-body px-lg-5 py-lg-5">
        <div class="text-center text-muted mb-4">
            <h3>Enter a valid mpesa number</h3>
        </div>
        <form role="form">
            <div class="form-group mb-3">
                <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-phone"></i></span>
                    </div>
                    <input class="form-control" placeholder="Number" type="number">
                </div>
            </div>
            <div class="text-center">
                <button type="button" class="btn btn-primary my-4">Pay with Mpesa</button>
            </div>
        </form>
    </div>
</div>



            </div>

        </div>
    </div>
</div>
  </div>
</div>

@endsection
