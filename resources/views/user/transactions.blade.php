@extends('layouts.user')

@section('content')
<div id="demo" class="carousel slide" data-ride="carousel">

  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="/img/golf.jpg" alt="" width="1100" height="500">
    </div>
  </div>
</div>

<section class="golf-website py-5">
  <h2 class="text-center text-uppercase"><span class="text-lowercase">Your </span>Transactions</h2>
</section>

<div id="accordion" style="margin-top: 40px">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Mpesa transactions of membership
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        Details on transactions
        time:
        price:
        package:
      </div>
    </div>
  </div>

@endsection
