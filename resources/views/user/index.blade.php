@extends('layouts.user')
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.the_fixed_bg {
  background-image: url('img/golf_course.jpg');
  max-height: 300px;
  background-attachment: fixed;
  background-repeat: no-repeat;
  width: inherit;
}
img{
  ::linear-gradient(rgba(255, 0, 0, 0.5);)
}
</style>
@endsection


@section('content')
<div id="demo" class="carousel slide" data-ride="carousel">

  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>

  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="img/login_golf.jpg"  width="1100" height="500">
    </div>
    <div class="carousel-item">
      <img src="img/golf-carousel.jpg" width="1100" height="500">
    </div>
    <div class="carousel-item">
      <img src="img/golf.jpg" width="1100" height="500">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>

  <section class="golf-website py-5">
    <h2 class="text-center text-uppercase"><span class="text-lowercase">Welcome to </span>Strolf</h2>
    <p class="text-center mt-4">The strathmore golf club ready to help perfect your swing.</p>
  </section>

    <div class="business-hours">
        <div class="container">
            <div class="row">
                <div class="col-md-6 py-5">
                  <h2 class="text-center text-uppercase">Training Hours</h2>
<p class="text-center mt-5">Morbi risus mi, feugiat non nulla eu, dapibus sagittis turpis. Praesent vel dignissim libero. Morbi vel porttitor orci. Integer non elit eu odio vulputate aliquet at sed odio.</p>
<table class="table table-hover text-center mt-5">
     <thead class="table-danger">
         <tr>
             <th class="text-center">Day</th>
             <th class="text-center">From</th>
             <th class="text-center">To</th>
         </tr>
     </thead>
     <tbody>
         <tr>
            <td>Tuesday</td>
             <td>17:00</td>
             <td>19:00</td>
         </tr>
         <tr>
             <td>Thursday</td>
             <td>17:00</td>
             <td>19:00</td>
         </tr>
     </tbody>
  </table>
</div>
  <div class="col-md-6 bg-hours">
    <img src="img/golf_balls.jpg" class="img-fluid">
  </div>
            </div>
        </div>
    </div>

<div class="container py-4">
<div class="row">
<main class="col-lg-8 main-content">
<h2 class="d-block d-md-none text-uppercase text-center">About Us</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed convallis ac risus sit amet condimentum. Duis pellentesque vitae erat a varius. Donec tincidunt, risus sit amet varius tincidunt, turpis arcu ullamcorper ligula, at feugiat turpis massa vitae erat. Nam sit amet posuere urna. Mauris consequat elit in tellus fringilla, in mollis dolor feugiat. Vivamus fringilla eros sed leo maximus rhoncus. Phasellus sit amet vehicula diam. Pellentesque ut lorem ex. Donec sed accumsan velit.</p>

<p>Vestibulum egestas neque eget est pharetra gravida. Vestibulum aliquet, mauris nec blandit pharetra, tellus lorem feugiat dui, non sollicitudin quam neque et lorem. Fusce quis magna sollicitudin, consectetur ante ut, imperdiet turpis. Fusce hendrerit justo ut bibendum semper. Quisque fringilla posuere neque a vulputate.</p>

</main>



<aside class="col-lg-4 pt-4 pt-lg-0">
<div class="sidebar hours p-3">
  <h2 class="text-center text-uppercase">Latest Golf gear</h2>
 <p class="text-center mt-5">Morbi risus mi, feugiat non nulla eu, dapibus sagittis turpis. Praesent vel dignissim libero. Morbi vel porttitor orci. Integer non elit eu odio vulputate aliquet at sed odio.</p>
 <table class="table table-hover text-center mt-5">
    <img class="rounded mx-auto d-block" style="width:300px;" width="inherit" src="img/golf_3.jpg">
 </table>
</div>
</aside>

<div class="the_fixed_bg img-fluid"></div>

</div>

</div>

<div class="appointment container-fluid py-5 bg-primary">
    <div class="row">
        <div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3 py-3 text-center ">
              <h3 class="text-uppercase">Join Us today</h3>
              <p> Maecenas rhoncus, augue sed volutpat suscipit, augue felis laoreet lectus, vel convallis diam est eu lectus. Mauris metus orci, tempus nec bibendum eget, pulvinar at metus. Etiam egestas sodales auctor.</p>
              <a href="#" class="btn btn-primary btn-lg mt-3 text-uppercase">read more</a>
        </div> <!--.col-12-->
    </div> <!--.row-->
</div>


@endsection
