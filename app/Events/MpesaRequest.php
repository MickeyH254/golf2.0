<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\MpesaResponse;

class MpesaRequest 
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $mpesaResponse;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(MpesaResponse $mpesaResponse)
    {
        //
        $this->MpesaResponse=$mpesaResponse;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
