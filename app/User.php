<?php

namespace App;

use Laravel\Cashier\Billable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Billable;
    use Notifiable;
    use HasApiTokens;

    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'image_url','date_of_birth','Occupation','phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
      return $this->belongsToMany('App\Role' ,'role_user', 'user_id', 'role_id');
    }
    public function salary(){
      return $this->hasOne('App\Salary');
    }
    public function checkRole($role){
      if (in_array($role,Auth::user()->roles()->select('roles.*')->pluck('name')->toArray())){
        return true;
      }
    }

    public function generateToken()
    {
        $this->api_token = str_random(60);
        $this->save();

        return $this->api_token;
    }
}
