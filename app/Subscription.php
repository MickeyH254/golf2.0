<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    //
    protected $fillable=['name','duration_in_months','price'];

    public function mpesarequests()
    {
      // code...
      return $this->hasMany('App\MpesaRequest','subscription_id'  , 'id');
    }
}
