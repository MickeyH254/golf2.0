<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    //
    protected $fillable=['name',
                        'amount',
                        "reccurence",
            "date_from",
            "date_to",
            "duration",
            'status'];

}
