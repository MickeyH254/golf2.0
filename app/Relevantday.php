<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relevantday extends Model
{
    //
    protected $fillable=['title','start','end','allDay','repeat'];
}
