<?php

namespace App;

// use Sofa\Eloquence\Eloquence;
use Illuminate\Database\Eloquent\Model;

class MpesaRequest extends Model
{
    protected $table = 'mpesa_requests';

    protected $fillable = [
      "user_id",
      "subscription_id",
      "MerchantRequestID",
      "CheckoutRequestID",
      "ResponseCode",
      "ResponseDescription",
      "CustomerMessage",
      "invoice_number"
     ];

    //Eloquence Search mapping
    // use Eloquence;
    public function subscription(){
      return $this->belongsTo('App\Subscription');
    }


}
