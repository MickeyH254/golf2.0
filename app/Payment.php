<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    protected $fillable=['user_id',
                          'subscription_id',
                          'mode',
                          'debit',
                          'credit',
                          'discount',
                          'for_the_month',
                          'start_date',
                          'expiry_date'];
public function users()
{
  // code...
  return $this->belongsTo('App\User');
}
}
