<?php

namespace App;

use Sofa\Eloquence\Eloquence;
use Illuminate\Database\Eloquent\Model;

class MpesaResponse extends Model
{
    protected $table = 'mpesa_responses';

    protected $fillable = [
      "MerchantRequestID",
        "CheckoutRequestID",
        "ResultCode",
        "ResultDesc",
        "Amount",
        "MpesaReceiptNumber",
        "TransactionDate",
        "PhoneNumber"
     ];

    //Eloquence Search mapping
    use Eloquence;


}
