<?php

namespace App\Listeners;

use Mail;
use App\Mail\PaymentMade;
use App\Events\MpesaRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;
use App\Payment;
use App\MpesaRequest;

class PutSuccessfulRequestIntoPaymentTable
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MpesaRequest  $event
     * @return void
     */
    public function handle(MpesaRequest $event)
    {
        //
        $mrq=MpesaRequest::where('CheckoutRequestID','=',$event->mpesaResponse->CheckoutRequestID);
        $sub=$mrq->subscription();
        $payment=new Payment();
        $payment->user_id=$mrq->user_id;
        $payment->subscription_id=$sub->id;
        $payment->amount=$event->mpesaResponse->Amount;
        $payment->mode="mpesa";
        $payment->debit=0;
        $payment->credit=0;
        $payment->discount=0;
        $payment->for_the_month=Carbon::now()->month;
        $payment->start_date= Carbon::now()->toDateString();
        $payment->expiry_date=Carbon::now()->addDays(30);
        $payment->save();

        Mail::to('jrarui.jr@gmail.com')->send(new PaymentMade(User::find($mrq->user_id),Carbon::now()->toDateString(),Carbon::now()->addDays(30)));



    }
}
