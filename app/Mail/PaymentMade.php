<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentMade extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $start_date;
    public $expiry_date;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$start_date,$expiry_date)
    {
        //
        $this->$user=$user;
        $this->$start_date=$start_date;
        $this->$expiry_date=$expiry_date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('cr',compact("user","start_date","expiry_date"));
    }
}
