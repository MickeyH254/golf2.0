<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    //
    protected $fillable=['date_collected',
                        "user_id",
                        "trainer_id"];
}
