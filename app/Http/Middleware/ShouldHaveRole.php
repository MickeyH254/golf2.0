<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class ShouldHaveRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role)
    {
      if (Auth::check() && Auth::user()->checkRole($role)) {
       return $next($request);

     }

     elseif (!Auth::check()) {
      return redirect('/login');

    }
    return redirect('/');
    }

}
