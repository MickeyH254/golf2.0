<?php

namespace App\Http\Controllers;

use App\Relevantday;
use Illuminate\Http\Request;

class RelevantdayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $relevantday=new Relevantday();

        $relevantday->title=$request->title;
        $relevantday->start=$request->start;
        $relevantday->end=$request->end;
        $relevantday->allDay=$request->allDay=="on"?1:0;
        $relevantday->repeat=$request->repeat=="on"?1:0;
        $relevantday->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Relevantday  $relevantday
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Relevantday  $relevantday
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Relevantday  $relevantday
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Relevantday  $relevantday
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
