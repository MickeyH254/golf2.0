<?php

namespace App\Http\Controllers;

use App\TrainerPayment;
use Illuminate\Http\Request;

class TrainerPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TrainerPayment  $trainerPayment
     * @return \Illuminate\Http\Response
     */
    public function show(TrainerPayment $trainerPayment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TrainerPayment  $trainerPayment
     * @return \Illuminate\Http\Response
     */
    public function edit(TrainerPayment $trainerPayment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TrainerPayment  $trainerPayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TrainerPayment $trainerPayment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TrainerPayment  $trainerPayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(TrainerPayment $trainerPayment)
    {
        //
    }
}
