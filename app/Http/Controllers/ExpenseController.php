<?php

namespace App\Http\Controllers;

use App\Expense;
use Illuminate\Http\Request;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $expenses=Expense::all();
        return $expenses;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $expense=new Expense();
        $expense->name=$request->name;
        $expense->amount=$request->amount;
        $expense->reccurence=$request->reccurence;
        $expense->date_from=$request->date_from;
        $expense->date_to=$request->date_to;
        $expense->duration=$request->duration;
        $expense->status=$request->status;
        $expense->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $expense=Expense::find($id);
        return $expense;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $expense=Expense::find($id);
        $expense->name=$request->name;
        $expense->amount=$request->amount;
        $expense->reccurence=$request->reccurence;
        $expense->date_from=$request->date_from;
        $expense->date_to=$request->date_to;
        $expense->duration=$request->duration;
        $expense->status=$request->status;
        $expense->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $expense=Expense::find($id);
        $expense->delete();
        return "Delete Successful";
    }
}
