<?php

namespace App\Http\Controllers;

use App\MonthlyExpense;
use Illuminate\Http\Request;

class MonthlyExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MonthlyExpense  $monthlyExpense
     * @return \Illuminate\Http\Response
     */
    public function show(MonthlyExpense $monthlyExpense)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MonthlyExpense  $monthlyExpense
     * @return \Illuminate\Http\Response
     */
    public function edit(MonthlyExpense $monthlyExpense)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MonthlyExpense  $monthlyExpense
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MonthlyExpense $monthlyExpense)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MonthlyExpense  $monthlyExpense
     * @return \Illuminate\Http\Response
     */
    public function destroy(MonthlyExpense $monthlyExpense)
    {
        //
    }
}
