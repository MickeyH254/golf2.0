<?php

namespace App\Http\Controllers;

use App\Attendance;
use Illuminate\Http\Request;
use Input;
use Carbon\Carbon;
use App\User;
use App\Payment;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $attendances=Attendance::all();
        return $attendances;
    }

    public function getForDate()
    {
        //

        $date=Input::get('date');
        $dat=Carbon::createFromFormat('m/d/Y', $date);
        $date=$dat->format('Y/m/d');
        $month=$dat->month;
        $attendances=Attendance::where('date_collected','=',$date)->get();
        $users=[];
        $absentees=[];
        $uids=[];
        $all=Payment::where('for_the_month','=',$month)->get();
        foreach ($attendances as $att) {
          // code...
          $x=User::find($att->user_id);
          $users[]=["name"=>$x->name,"id"=>$x->id,"att_id"=>$att->id];
          $uids[]=$att->user_id;

        }
        foreach ($all as $al) {
          // code...
          if (!contains($al->user_id,$uids)) {
            // code...
            $y=User::find($al->user_id);
            $absentees[]=["name"=>$y->name,"id"=>$y->id];
          }


        }
        return json_encode(compact('users','absentees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $attendance=new Attendance();
        $attendance->date_collected=$request->date_collected;
        $attendance->user_id=$request->user_id;
        $attendance->trainer_id=$request->trainer_id;
        $attendance->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $attendance=Attendance::find($id);
        return $attendance;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $attendance=Attendance::find($id);
        $attendance->date_collected=$request->date_collected;
        $attendance->user_id=$request->user_id;
        $attendance->trainer_id=$request->trainer_id;
        $attendance->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $attendance=Attendance::find($id);
        $attendance->delete();

    }
}
