<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Payment;
use Carbon\Carbon;
use App\Expense;
use Auth;
class TrainersController extends Controller
{
    //
    public function index()
    {
    $trainers=Role::where("name","=","member")->first()->users()->get();
    $totalMembers=count(Role::where('name','=','member')->first()->users()->get()->toArray());
    $month=Carbon::now()->month;
    $membersSinceLastMonth=count(Role::where('name','=','member')->first()->users()->get()->toArray())>0?(((count(Role::where('name','=','member')->first()->users()->get()->toArray())-count(Role::where('name','=','member')->first()->users()->where('users.created_at','<',Carbon::now()->month)->get()->toArray()))/(count(Role::where('name','=','member')->first()->users()->get()->toArray())))*100):0;
    $subscribedMembers=count(Payment::where('for_the_month','=',$month)->get());
    $totalAmount=array_sum(Payment::where('for_the_month','=',$month)->get(['amount'])->toArray());
    $totalExpenses=array_sum(Expense::where('date_to','<',Carbon::now())->get(['amount'])->toArray());
    $user=(new UserController())->show(Auth::user()->id);

    // $current_payments=Payment::where('for_the_month','=',Carbon::now()->month)->get();
    // $paidtrainers=[];
    // foreach ($current_payments as $current_payment) {
    //   // code...
    //   $paidtrainers[]=$current_payment->users()->get();
    // }
    return view("admin.trainers",compact('trainers','subscribedMembers','totalExpenses','user','membersSinceLastMonth','totalAmount','totalMembers'));
  }
}
