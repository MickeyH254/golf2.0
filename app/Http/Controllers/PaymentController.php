<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $payments=Payment::all();
        return $payments;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $payment=new Payment();
        $payment->user_id=$request->user_id;
        $payment->subscription_id=$request->subscription_id;
        $payment->mode=$request->mode;
        $payment->debit=$request->debit;
        $payment->credit=$request->credit;
        $payment->discount=$request->discount;
        $payment->for_the_month=$request->for_the_month;
        $payment->start_date=$request->start_date;
        $payment->expiry_date=$request->expiry_date;
        $payment->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $payment=Payment::find($id);
        return $payment;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $payment=Payment::find($id);
        $payment->user_id=$request->user_id;
        $payment->subscription_id=$request->subscription_id;
        $payment->mode=$request->mode;
        $payment->debit=$request->debit;
        $payment->credit=$request->credit;
        $payment->discount=$request->discount;
        $payment->for_the_month=$request->for_the_month;
        $payment->start_date=$request->start_date;
        $payment->expiry_date=$request->expiry_date;
        $payment->update();
        return "save successful";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $payment=Payment::find($id);
        $payment->delete();
        return "deleted successfully";
    }
}
