<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class ClientController extends Controller
{
    //
    public function index()
    {
      // code...
      // $totalMembers=count(Role::where('name','=','member')->first()->users()->get()->toArray());
      // $month=Carbon::now()->month;
      // $membersSinceLastMonth=count(Role::where('name','=','member')->first()->users()->get()->toArray())>0?(((count(Role::where('name','=','member')->first()->users()->get()->toArray())-count(Role::where('name','=','member')->first()->users()->where('users.created_at','<',Carbon::now()->month)->get()->toArray()))/(count(Role::where('name','=','member')->first()->users()->get()->toArray())))*100):0;
      // $subscribedMembers=count(Payment::where('for_the_month','=',$month)->get());
      // $totalAmount=array_sum(Payment::where('for_the_month','=',$month)->get(['amount'])->toArray());
      // $totalExpenses=array_sum(Expense::where('date_to','<',Carbon::now())->get(['amount'])->toArray());
      // $user=(new UserController())->show(Auth::user()->id);

      return view('user.index');
    }
    public function payment()
    {
      // code...
      return view('user.payment');
    }
    public function profile()
    {
      // code...
      $user=Auth::user();
      return view('user.profile',compact('user'));
    }
    public function transactions()
    {
      // code...
      return view('user.transactions');
    }
}
