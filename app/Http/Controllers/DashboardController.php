<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use App\Role;
use App\User;
use Carbon\Carbon;
use Auth;
use App\Expense;

class DashboardController extends Controller
{
    //
    public function __construct()
    {
        // $this->middleware('hasRole:admin');
    }

    public function index()
    {
        //
        $totalMembers=count(Role::where('name','=','member')->first()->users()->get()->toArray());
        $month=Carbon::now()->month;
        $membersSinceLastMonth=count(Role::where('name','=','member')->first()->users()->get()->toArray())>0?(((count(Role::where('name','=','member')->first()->users()->get()->toArray())-count(Role::where('name','=','member')->first()->users()->where('users.created_at','<',Carbon::now()->month)->get()->toArray()))/(count(Role::where('name','=','member')->first()->users()->get()->toArray())))*100):0;
        $subscribedMembers=count(Payment::where('for_the_month','=',$month)->get());
        $totalAmount=array_sum(Payment::where('for_the_month','=',$month)->get(['amount'])->toArray());
        $totalExpenses=array_sum(Expense::where('date_to','<',Carbon::now())->get(['amount'])->toArray());
        $user=(new UserController())->show(Auth::user()->id);

        return view('admin.index',compact('subscribedMembers','totalExpenses','user','membersSinceLastMonth','totalAmount','totalMembers'));
    }
    public function adduser()
    {
      // code...
      $totalMembers=count(Role::where('name','=','member')->first()->users()->get()->toArray());
      $month=Carbon::now()->month;
      $membersSinceLastMonth=count(Role::where('name','=','member')->first()->users()->get()->toArray())>0?(((count(Role::where('name','=','member')->first()->users()->get()->toArray())-count(Role::where('name','=','member')->first()->users()->where('users.created_at','<',Carbon::now()->month)->get()->toArray()))/(count(Role::where('name','=','member')->first()->users()->get()->toArray())))*100):0;
      $subscribedMembers=count(Payment::where('for_the_month','=',$month)->get());
      $totalAmount=array_sum(Payment::where('for_the_month','=',$month)->get(['amount'])->toArray());
      $totalExpenses=array_sum(Expense::where('date_to','<',Carbon::now())->get(['amount'])->toArray());
      $roles=Role::all();
      $user=(new UserController())->show(Auth::user()->id);
      return view('admin.register',compact('subscribedMembers','totalExpenses','user','membersSinceLastMonth','totalAmount','totalMembers','roles'));
    }
}
