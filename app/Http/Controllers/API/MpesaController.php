<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Subscription;
use Auth;
use Input;
// use Request;
use App\MpesaRequest;
use App\MpesaResponse;
use \Illuminate\Http\Request;

class MpesaController extends Controller
{
  public function index(){
    return MpesaResponse::all();
  }
 
  public function request(Request $request){
    $ac_to=\Request::get('ac_to');
    $subscription_id=\App\Subscription::find($request->subscription_id);

    // $this->generateOauth();
    $ur=env("MPESA_PAYMENT_URL");
    $curl = curl_init();
    $timestamp=str_replace("-","",str_replace(" ","",str_replace(":","",trim(Carbon::now()))));
    curl_setopt($curl, CURLOPT_URL, $ur);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$ac_to)); //setting custom header


    $curl_post_data = array(
    //Fill in the request parameters with valid values
    'BusinessShortCode' => env("MPESA_SHORTCODE"),
    'Password' => base64_encode(env("MPESA_SHORTCODE").env("MPESA_PASSWORD").$timestamp),
    'Timestamp' => $timestamp,
    'TransactionType' => 'CustomerPayBillOnline',
    'Amount' => $subscription_id->price,
    'PartyA' => $request->cust_phone,
    'PartyB' => env("MPESA_SHORTCODE"),
    'PhoneNumber' => $request->cust_phone,
    'CallBackURL' => env("MPESA_CALLBACK_URL"),
    'AccountReference' => $subscription_id->name,
    'TransactionDesc' => " 1 month"
    );

    $data_string = json_encode($curl_post_data);

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

    $curl_response = curl_exec($curl);
    // print_r($curl_response);
    $cr=json_decode($curl_response, true);

    $crmpesa = [
        "user_id"=>Auth::user()->id,
        "subscription_id"=>$subscription_id->id,
        "MerchantRequestID"=>$cr["MerchantRequestID"],
            "CheckoutRequestID"=>$cr["CheckoutRequestID"],
            "ResponseCode"=>$cr["ResponseCode"],
            "ResponseDescription"=>$cr["ResponseDescription"],
            "CustomerMessage"=>$cr["CustomerMessage"]
            ];
            // "invoice_number"=>Input::get('invoice_id')

    $mr=new MpesaRequest($crmpesa);
    $mr->save();

    return $curl_response;
  }


  public function response(){
    // error_log(json_decode(Request::instance()->getContent(),true)['Body']['stkCallback']['ResultCode']);
    $cr=json_decode(Request::instance()->getContent(),true);
    $crmpesa = [
      "MerchantRequestID"=>$cr['Body']['stkCallback']["MerchantRequestID"],
            "CheckoutRequestID"=>$cr['Body']['stkCallback']["CheckoutRequestID"],
            "ResultCode"=>$cr['Body']['stkCallback']["ResultCode"],
            "ResultDesc"=>$cr['Body']['stkCallback']["ResultDesc"]

    ];
    if ($cr['Body']['stkCallback']["ResultCode"]==0) {
      // code...
      $crmpesa=array_merge([
        "Amount"=>$cr['Body']['stkCallback']['CallbackMetadata']['Item'][0]['Value'],
        "MpesaReceiptNumber"=>$cr['Body']['stkCallback']['CallbackMetadata']['Item'][1]['Value'],
        "TransactionDate"=>$cr['Body']['stkCallback']['CallbackMetadata']['Item'][3]['Value'],
        "PhoneNumber"=>$cr['Body']['stkCallback']['CallbackMetadata']['Item'][4]['Value']
      ],$crmpesa);
      // error_log(json_encode($cr['Body']['stkCallback']['CallbackMetadata']['Item']));
    }

    $mr=new MpesaResponse($crmpesa);
    $mr->save();

    $payment=new Payment();
    $user_id=MpesaRequest::where("CheckoutRequestID","=",$cr['Body']['stkCallback']["CheckoutRequestID"])->get('user_id')->first();



  }
}
 ?>
