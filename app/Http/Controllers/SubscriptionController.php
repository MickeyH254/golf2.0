<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscription;

class SubscriptionController extends Controller
{
    //
    public function index()
    {
        //
        $subscriptions= Subscription::all();
        return $subscriptions;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
          'name'=>'required','duration_in_months'=>'required','price'=>'required'
        ]);

        $subscription=new Subscription();
        $subscription->name=$request->name;
        $subscription->duration_in_months=$request->duration_in_months;
        $subscription->price=$request->price;
        $subscription->save();
        return "Saved successfully";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $subscription=Subscription::find($id);
        return $subscription;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
          'name'=>'required','duration_in_months'=>'required','price'=>'required'
        ]);
        $subscription=Subscription::find($id);
        $subscription->name=$request->name;
        $subscription->duration_in_months=$request->duration_in_months;
        $subscription->price=$request->price;
        $subscription->update();
        return "Update successful";

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $subscription=Subscription::find($id);
        $subscription->delete();
    }
}
