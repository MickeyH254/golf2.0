<?php

namespace App\Http\Controllers;

use App\Salary;
use Illuminate\Http\Request;

class SalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         //
         $salarys=Salary::all();
         return $salarys;
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
         //
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {
         //

         $salary=new Salary();

         $salary->user_id=$request->user_id;
         $salary->amount=$request->amount;
         $salary->save();
         return "Save Success";
     }

     /**
      * Display the specified resource.
      *
      * @param  \App\Salary  $salary
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
         //
         $salary=Salary::find($id);
         return $salary;
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  \App\Salary  $salary
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
         //
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  \App\Salary  $salary
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {
         //
         $salary=Salary::find($id);
         $salary->user_id=$request->user_id;
         $salary->amount=$request->amount;
         $salary->update();
         return "update successful";
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  \App\Salary  $salary
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
         //
         $salary=Salary::find($id);
         $salary->delete();
         return "delete successful";
     }
}
