<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use App\Role;
use App\User;
use Carbon\Carbon;
use Auth;
use App\Expense;

class TrainerController extends Controller
{
    //
    public function index()
    {
      // code...
      $month=Carbon::now()->month;
      $lastmonth=Carbon::now()->subMonth()->month;
      $trainees=count(Payment::where('for_the_month','=',$month)->get());
      $traineesSinceLastMonth=count(Payment::where('for_the_month','=',$month)->get())>0?(((count(Payment::where('for_the_month','=',$month)->get())-count(Payment::where('for_the_month','=',$lastmonth)->get()))/(count(Payment::where('for_the_month','=',$month)->get())))*100):0;
      return view('trainers.members',compact('trainees','traineesSinceLastMonth'));
    }
    public function attend(){
      $month=Carbon::now()->month;
      $lastmonth=Carbon::now()->subMonth()->month;
      $trainees=count(Payment::where('for_the_month','=',$month)->get());
      $traineesSinceLastMonth=count(Payment::where('for_the_month','=',$month)->get())>0?(((count(Payment::where('for_the_month','=',$month)->get())-count(Payment::where('for_the_month','=',$lastmonth)->get()))/(count(Payment::where('for_the_month','=',$month)->get())))*100):0;
      return view('trainers.attendance',compact('trainees','traineesSinceLastMonth'));
    }
}
