<?php

namespace App\Http\Controllers;

use App\Payment;
use Carbon\Carbon;
use Auth;
use App\Role;
use App\Expense;

use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $totalMembers=count(Role::where('name','=','member')->first()->users()->get()->toArray());
        $month=Carbon::now()->month;
        $membersSinceLastMonth=count(Role::where('name','=','member')->first()->users()->get()->toArray())>0?(((count(Role::where('name','=','member')->first()->users()->get()->toArray())-count(Role::where('name','=','member')->first()->users()->where('users.created_at','<',Carbon::now()->month)->get()->toArray()))/(count(Role::where('name','=','member')->first()->users()->get()->toArray())))*100):0;
        $subscribedMembers=count(Payment::where('for_the_month','=',$month)->get());
        $totalAmount=array_sum(Payment::where('for_the_month','=',$month)->get(['amount'])->toArray());
        $totalExpenses=array_sum(Expense::where('date_to','<',Carbon::now())->get(['amount'])->toArray());
        $user=(new UserController())->show(Auth::user()->id);
        $payments=Payment::all();
        return view('admin.transactions',compact('payments','subscribedMembers','totalExpenses','user','membersSinceLastMonth','totalAmount','totalMembers'));
    }


}
