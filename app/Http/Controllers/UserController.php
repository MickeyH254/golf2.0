<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      //
      $users=User::with('roles')->get();

      return $users;
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */


  public function create()
  {
      //
  }


  public function fileUpload($image)

{

    $this->validate($request, [

        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

    ]);




    $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

    $destinationPath = public_path('/images');

    $image->move($destinationPath, $input['imagename']);


    // $this->postImage->add($input);

    return $input['imagename'];
    // return back()->with('success','Image Upload successful');


}

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      //
      $user= new User();
      $this->validate($request, [

          'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

      ]);
      $user->name=$request->name;
      $user->email=$request->email;
      $user->password=Hash::make($request->password);

      if($request->date_of_birth!=null){
      $user->date_of_birth=$request->date_of_birth;
    }
      if($request->Occupation!=null){
      $user->Occupation=$request->Occupation;
    }
    if($request->phone!=null){
      $user->phone=$request->phone;
    }
      if($request->file('image')!==null){
      $image = $request->file('image');
      $user->image_url=fileUpload($image);
    }
    $user->save();
    if($request->role_id!=null){
      $user->roles()->attach($request->role_id);
    }
    return "Saved Successfully";
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
      $user=User::find($id);
      $role=$user->roles();
      return ["user"=>$user,"role"=>$role];
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $user=User::find($id);
    $this->validate($request, [

        'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

    ]);
    if($request->name!=null){
      $user->name=$request->name;
    }
    if($request->phone!=null){
      $user->phone=$request->phone;
    }

    if($request->email!=null){
      $user->email=$request->email;
    }

    if($request->password!=null){
      $user->password=Hash::make($request->password);
    }

    if($request->date_of_birth!=null){
    $user->date_of_birth=$request->date_of_birth;
  }
    if($request->Occupation!=null){
    $user->Occupation=$request->Occupation;
  }
    if($request->file('image')!==null){
    $image = $request->file('image');
    $user->image_url=fileUpload($image);
  }
  $user->update();
  if($request->role_id!=null){
    $user->roles()->attach($request->role_id);
  }
  return "update successful";
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //
      $user=User::find($id);
      $user->sync();
      $user->delete();
  }
}
