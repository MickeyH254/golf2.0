<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/login', function () {
    return view('auth.login');
})->name('login');
Route::get('/adduser', 'DashboardController@adduser')->name('admin-register');
Route::post('login', 'Auth\LoginController@login');

Route::get('/admin', 'DashboardController@index')->name('admin');
Route::post('/adduser', 'UserController@store')->name('add-user');
Route::get('/members', 'MembersController@index')->name('members');
Route::get('/trainer', 'TrainerController@index')->name('trainer');
Route::get('/attend', 'TrainerController@attend')->name('attend');
Route::get('/trainers', 'TrainersController@index')->name('trainers');
Route::get('/profile', 'ProfileController@index')->name('profile');
Route::post('/user/{id}', 'UserController@update')->name('post_user');
Route::get('/calendar', 'CalendarController@index')->name('Calendar');
Route::get('/transaction', 'TransactionController@index')->name('transactions');
Route::get('/attendees','AttendanceController@getForDate')->name('attendees');
Route::group(['prefix'=>'mpesa', 'middleware' => ['MpesaOauth']],function (){
   Route::post('/', 'MpesaController@request');

 });

Route::group(['prefix'=>'user'],function (){
  Route::get('/','UserController@index');

});
Route::group(['prefix'=>'member'],function (){
  Route::get('/','ClientController@index');
  Route::get('payment','ClientController@payment');
  Route::get('profile','ClientController@profile');
  Route::get('transactions','ClientController@transactions');

});
Route::get('mpesa/response','MpesaController@response');
Route::get('mpesa/payments','MpesaController@index');
Route::post('mpesa/response','MpesaController@response');
Route::post('relday','RelevantdayController@store');
