<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::post('register', 'Auth\RegisterController@register');
// Route::post('login', 'Auth\LoginController@login');

Route::post('login', 'API\PassportController@login');
Route::post('register', 'API\PassportController@register');

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('payments', 'API\PaymentController@index');
    Route::get('payments/{payment}', 'API\PaymentController@show');
    Route::post('payments', 'API\PaymentController@store');
    Route::put('payments/{payment}', 'API\PaymentController@update');
    Route::delete('payments/{payment}', 'API\PaymentController@delete');

    Route::get('attendance', 'API\AttendanceController@index');
    Route::get('attendance/{attendance}', 'API\AttendanceController@show');
    Route::post('attendance', 'API\AttendanceController@store');
    Route::put('attendance/{attendance}', 'API\AttendanceController@update');
    Route::delete('attendance/{attendance}', 'API\AttendanceController@delete');

    Route::get('relevantdays', 'API\RelevantDaysController@index');
    Route::get('relevantdays/{relevantdays}', 'API\RelevantDaysController@show');
    Route::post('relevantdays', 'API\RelevantDaysController@store');
    Route::put('relevantdays/{relevantdays}', 'API\RelevantDaysController@update');
    Route::delete('relevantdays/{relevantdays}', 'API\RelevantDaysController@delete');

    Route::post('logout', 'API\PassportController@logout');

    Route::group(['prefix'=>'mpesa', 'middleware' => ['MpesaOauth']],function (){
        //Route::post('/', 'MpesaController@request');
        //Route::get('/' , 'API\MpesaController@index');
        Route::post('/' , 'API\MpesaController@request');
     
      });
     

    


});