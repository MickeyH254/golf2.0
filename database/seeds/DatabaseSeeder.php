<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\Role_user;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->call(RolesTableSeeder::class);
       $this->command->info('Roles table seeded!');
       $this->call(UsersTableSeeder::class);
       $this->command->info('Users table seeded!');
       $this->call(UserRolesTableSeeder::class);
         $this->command->info('User roles table seeded!');
        // $this->call(UsersTableSeeder::class);
    }
}
class UsersTableSeeder extends Seeder{
  public function run()
   {
     DB::table('users')->delete();
     User::create(['name'=>"Developer", 'email'=>"jrarui@strathmore.edu", 'password'=>bcrypt("CENTURION1."),'image_url'=>"/img/avatar.png",'date_of_birth'=>"1999-02-20",'Occupation'=>"Developer",'phone'=>"+254741459354"]);
     User::create(['name'=>"mgichure", 'email'=>"mgichure@strathmore.edu", 'password'=>bcrypt("Golfadmin2019"),'image_url'=>"/img/avatar.png",'date_of_birth'=>"1999-02-20",'Occupation'=>"Super-Admin",'phone'=>"+254725165078"]);
     User::create(['name'=>"smaritim", 'email'=>"sheila.maritim716@strathmore.edu", 'password'=>bcrypt("Golfmanager2019"),'image_url'=>"/img/avatar.png",'date_of_birth'=>"1999-02-20",'Occupation'=>"Manager",'phone'=>"+254727342627"]);
   }
}
class RolesTableSeeder extends Seeder {

   public function run()
   {
       DB::table('roles')->delete();

       Role::create(array("name"=>'admin','display_name'=>"admin",'description' => 'This is the super user in the system with full rights'));
       Role::create(array("name"=>'member','display_name'=>"member",'description' => 'This is the normal user in the system who can only reserve and view his reservations'));
       Role::create(array("name"=>'trainer','display_name'=>"trainer",'description' => 'This is for the users in the system who can approve reservations'));
   }

}
class UserRolesTableSeeder extends Seeder {

   public function run()
   {
       DB::table('role_user')->delete();

      Role_user::create(array("user_id"=>1,'role_id' => 1));
       Role_user::create(array("user_id"=>2,'role_id' => 1));
       Role_user::create(array("user_id"=>3,'role_id' => 1));
   }

}
