<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainerPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainer_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("expense_id");
            $table->foreign("expense_id")->references('id')->on('expenses');
            $table->unsignedInteger("user_id");
            $table->foreign("user_id")->references('id')->on("users");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainer_payments');
    }
}
