<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpesaResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpesa_responses', function (Blueprint $table) {
            $table->increments('id');
            $table->string("MerchantRequestID");
            $table->string("CheckoutRequestID");
            $table->string("ResultCode");
            $table->text("ResultDesc");
            $table->double("Amount");
            $table->string("MpesaReceiptNumber");
            $table->string("TransactionDate");
            $table->string("PhoneNumber");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpesa_responses');
    }
}
